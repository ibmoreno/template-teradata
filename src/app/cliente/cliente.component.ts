import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

export interface Usuario {
  id: number;
  nome: String;
  sexo: String;
  categoria: String;
  dataNascimento: String;
}

const DATA_SOURCE_USUARIOS: Usuario[] = [
  { id: 1, nome: 'IVAN BATISTA MORENO', sexo: 'M', categoria: 'COMERCIÁRIO', dataNascimento: '28/05/1977' },
  { id: 2, nome: 'REINALDO DE ARRUDA LIMA', sexo: 'M', categoria: 'COMERCIÁRIO', dataNascimento: '01/01/1975' },
  { id: 3, nome: 'MARIA APARECIDA MORENO', sexo: 'F', categoria: 'USUÁRIO', dataNascimento: '28/11/1965' },
  { id: 4, nome: 'BRUNO DE OLIVEIRA FIGUEIREDO', sexo: 'M', categoria: 'DEPENDENTE', dataNascimento: '25/05/1981' },
  { id: 5, nome: 'LUCIVANIA APARECIDA FARIAS', sexo: 'F', categoria: 'DEPENDENTE', dataNascimento: '05/03/1981' },
  { id: 6, nome: 'ANDRÉ LUIZ DA SILVA', sexo: 'M', categoria: 'COMERCIÁRIO', dataNascimento: '07/10/1982' },
  { id: 7, nome: 'ANTONIO BATISTA DE SOUZA', sexo: 'M', categoria: 'COMERCIÁRIO', dataNascimento: '10/06/1970' },
  { id: 8, nome: 'ALICE SOUZA BATISTA MORENO', sexo: 'F', categoria: 'USUÁRIO', dataNascimento: '03/02/1974' },
  { id: 9, nome: 'BENEDITA DA SILVA FILHO', sexo: 'F', categoria: 'COMERCIÁRIO', dataNascimento: '30/07/1966' },
  { id: 10, nome: 'SILVAN BATISTA MORENO', sexo: 'M', categoria: 'COMERCIÁRIO', dataNascimento: '21/07/1972' },
  { id: 11, nome: 'RONALDO BATISTA MORENO', sexo: 'M', categoria: 'COMERCIÁRIO', dataNascimento: '28/05/1977' },
  { id: 12, nome: 'JOSÉ DE ARRUDA LIMA', sexo: 'M', categoria: 'COMERCIÁRIO', dataNascimento: '01/01/1975' },
  { id: 13, nome: 'APARECIDA FILHO MORENO', sexo: 'F', categoria: 'COMERCIÁRIO', dataNascimento: '28/11/1965' },
  { id: 14, nome: 'MARIO DE OLIVEIRA FIGUEIREDO', sexo: 'M', categoria: 'DEPENDENTE', dataNascimento: '25/05/1981' },
  { id: 15, nome: 'LUCI APARECIDA FARIAS', sexo: 'F', categoria: 'USUÁRIO', dataNascimento: '05/03/1981' },
  { id: 16, nome: 'ROBERTO LUIZ DA SILVA', sexo: 'M', categoria: 'USUÁRIO', dataNascimento: '07/10/1982' },
  { id: 17, nome: 'JOÃO BATISTA DE SOUZA', sexo: 'M', categoria: 'COMERCIÁRIO', dataNascimento: '10/06/1970' },
  { id: 18, nome: 'ANTONIA DA SILVA FILHO', sexo: 'F', categoria: 'COMERCIÁRIO', dataNascimento: '03/02/1974' },
  { id: 19, nome: 'JOSE MARIA DA SILVA FILHO', sexo: 'M', categoria: 'COMERCIÁRIO', dataNascimento: '30/07/1966' },
  { id: 20, nome: 'MARCIO JOSE GONÇALVES', sexo: 'M', categoria: 'USUÁRIO', dataNascimento: '21/07/1972' },
];


@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.scss']
})
export class ClienteComponent implements OnInit {

  displayedColumns: string[] = ['id', 'nome', 'sexo', 'categoria', 'dataNascimento', 'controle'];
  dataSource = new MatTableDataSource<Usuario>(DATA_SOURCE_USUARIOS);
  value = '';

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor() {
  }


  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    // traduz os label dos controles do datatable
    this.paginator._intl.firstPageLabel = 'Primeira Página';
    this.paginator._intl.previousPageLabel = 'Página Anterior';
    this.paginator._intl.nextPageLabel = 'Próxima Página';
    this.paginator._intl.lastPageLabel = 'Última Página';
    this.paginator._intl.itemsPerPageLabel = 'Itens por página';
  }


  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage;
    }
  }


}

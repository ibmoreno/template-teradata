import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(
    private _iconRegistry: MatIconRegistry,
    private _domSanitizer: DomSanitizer) {

    this._iconRegistry.addSvgIconInNamespace('assets',
      'teradata-dark', this._domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/teradata-dark.svg'));
    this._iconRegistry.addSvgIconInNamespace('assets',
      'teradata', this._domSanitizer.bypassSecurityTrustResourceUrl('/assets/icons/teradata.svg'));

  }

  ngOnInit(){

  }

}
